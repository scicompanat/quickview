import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import math

try:
    import PyCA.Common as common
    HAS_PyCA = True
except ImportError:
    HAS_PyCA = False
    

from scipy import spatial

# Mapping from dimension to dim index
dimmap = {'x':0, 'y':1, 'z':2,
          0:0, 1:1, 2:2}

class NumpyDef(object):
    """
    Dense deformation, implements getDefPoint and getDefOffset
    """

    def __init__(self, defArr):
        # takes x-by-y-by-2 or x-by-y-by-z-by-3 array
        if len(defArr.shape) == 3:
            assert defArr.shape[2] == 2
            defArr = np.append(defArr,
                               np.zeros((defArr.shape[0], defArr.shape[1], 1)),
                               axis=2)
            defArr = defArr[:,:,None,:]
        elif len(defArr.shape) == 4:
            assert defArr.shape[3] == 3
        else:
            raise Exception('error, incorrect shape for vector field: %s',
                            str(defArr.shape))
        self.vf = self.isVF(defArr)
        self.ID = self.getID(defArr)
        self.defArr = defArr

    def getID(self, defArr):
        sz = defArr.shape
        ID = np.mgrid[:sz[0],:sz[1],:sz[2]]
        ID = np.concatenate([np.squeeze(arr, axis=0)[:,:,:,None]
                             for arr in np.vsplit(ID, ID.shape[0])],
                            axis=3)
        return ID

    def isVF(self, defArr):
        ID = self.getID(defArr)
        return np.sum(np.abs(defArr)) < np.sum(np.abs(defArr-ID))
            
    def getDefPoint(self, pos):
        ipos = np.array(pos).astype('int')
        ipos = np.maximum(ipos, np.array([0., 0., 0.]))
        ipos = np.minimum(ipos, np.array(self.defArr.shape[:3])-1)
            
        return ipos

    def getDefOffset(self, ipos):
        ipos = ipos.astype('int')
            
        for dim in range(3):
            assert ipos[dim] >= 0 and ipos[dim] < self.defArr.shape[dim]

        off = self.defArr[ipos[0], ipos[1], ipos[2], :].copy()
        if not self.vf:
            off -= self.ID[ipos[0], ipos[1], ipos[2],:]

        return np.array([off[0], off[1], off[2]])
        # return off

class DefArrow(QtGui.QGraphicsItem):
    """
    GraphicsItem that can draw an arrow, a location, or both
    """

    def __init__(self,
                 arrowhead_len=3,
                 arrowhead_angle=math.pi/6.0,
                 draw_arrow=True,
                 draw_pos=False,
                 pos_rad=0.5,
                 pos_xrad=0.5,
                 pos = [0, 0],
                 off = [0., 0.]):
        super(DefArrow, self).__init__()
        self.arrowhead_len = arrowhead_len
        self.arrowhead_angle = arrowhead_angle
        self.rad = pos_rad
        self.xrad = pos_xrad
        self._show_arrow = draw_arrow
        self._show_pos = draw_pos
        self.pos = [0., 0.]
        self.off = [0., 0.]
        self.set_pos(pos)
        self.set_off(off)

    def show_arrow(self, show=True):
        if show != self._show_arrow:
            self._show_arrow = show
            self.update(self.boundingRect())

    def show_pos(self, show=True):
        if show != self._show_pos:
            self._show_pos = show
            self.update(self.boundingRect())

    def _draw_arrow(self, p, color='g'):

        if self.off[0] == 0.0 and self.off[1] == 0.0:
            p.setPen(pg.mkPen(color))
            # draw an 'x' of the size of the arrowhead
            hl = self.arrowhead_len/2.0
            p1 = QtCore.QPointF(hl, hl)
            p2 = QtCore.QPointF(-hl, -hl)
            p.drawLine(p1, p2)
            p3 = QtCore.QPointF(-hl, hl)
            p4 = QtCore.QPointF(hl, -hl)
            p.drawLine(p3, p4)
        else:
            # line showing offset
            p.setPen(pg.mkPen(color))
            p0 = pg.Point(0.0, 0.0)
            p1 = pg.Point(self.off[0], self.off[1])
            p.drawLine(p0, p1)

            # arrowhead lines
            theta = math.atan2(self.off[1], self.off[0])

            ah_x = self.arrowhead_len*math.cos(theta + math.pi -
                                               self.arrowhead_angle)
            ah_y = self.arrowhead_len*math.sin(theta + math.pi -
                                               self.arrowhead_angle)
            ah = pg.Point(ah_x, ah_y)
            p.drawLine(p1, p1+ah)

            ah_x = self.arrowhead_len*math.cos(theta - math.pi +
                                               self.arrowhead_angle)
            ah_y = self.arrowhead_len*math.sin(theta - math.pi +
                                               self.arrowhead_angle)
            ah = pg.Point(ah_x, ah_y)
            p.drawLine(p1, p1+ah)

    def _arrow_bound(self):
        return QtCore.QRectF(min(self.off[0], 0)-self.arrowhead_len,
                             min(self.off[1], 0)-self.arrowhead_len,
                             np.abs(self.off[0])+2*self.arrowhead_len,
                             np.abs(self.off[1])+2*self.arrowhead_len)
            
    def _draw_pos(self, p, color='r'):
        r = self.rad
        rx = self.xrad

        p.setPen(pg.mkPen('r'))
        p1 = QtCore.QPointF(rx, rx)
        p2 = QtCore.QPointF(-rx, -rx)
        p.drawLine(p1, p2)
        p3 = QtCore.QPointF(-rx, rx)
        p4 = QtCore.QPointF(rx, -rx)
        p.drawLine(p3, p4)

        p.drawRect(QtCore.QRectF(-r, -r, 2*r, 2*r))
    
    def _pos_bound(self):
        r = max(self.rad, self.xrad)
        return QtCore.QRectF(-r, -r, 2*r, 2*r)
    
    def paint(self, p, *args):
        if self._show_arrow:
            self._draw_arrow(p, color='g')
        if self._show_pos:
            self._draw_pos(p, color='r')

    def boundingRect(self):
        # rect = None
        # if self._show_arrow:
        #     r = self._arrow_bound()
        #     if rect is None:
        #         rect = r
        #     else:
        #         rect = rect.united(r)
        # if self._show_pos:
        #     r = self._pos_bound()
        #     if rect is None:
        #         rect = r
        #     else:
        #         rect = rect.united(r)
        # # final case
        # if rect is None:
        #     rect = QtCore.QRectF(0., 0., 0., 0.)
        rect = self._arrow_bound()
        rect = rect.united(self._pos_bound())

        return rect

    def set_pos(self, pos):
        pos = list(pos)
        if pos != self.pos:
            self.pos = pos
            p = pg.Point(int(self.pos[0])+0.5, int(self.pos[1])+0.5)
            self.setPos(p)

    def set_off(self, off):
        off = list(off)
        if self.off != off:
            cur_rect = self.boundingRect()
            self.off = off
            new_rect = self.boundingRect()
            self.update(cur_rect.united(new_rect))
        
class MovingArrow(pg.GraphicsObject):
    """
    QGraphicsObject that has a DefArrow as a child and moves the
    location based on signals
    """

    def __init__(self,
                 defOb,
                 arrowhead_len=3,
                 arrowhead_angle=math.pi/6.0):
        super(MovingArrow, self).__init__()
        self.arrow = DefArrow(arrowhead_len=arrowhead_len,
                              arrowhead_angle=arrowhead_angle,
                              draw_arrow=True,
                              draw_pos=False)
        self.arrow.setParentItem(self)
        self.pos = [0,0,0]
        self.off = [0,0,0]
        self.sdim = 2
        self.setDefOb(defOb)

    def setDefOb(self, defOb):
        if type(defOb) is np.ndarray:
                defOb = NumpyDef(defOb)
        elif type(defOb) is tuple:
            defOb = SparseDef(corrPos=defOb[0], corrMatch=defOb[1])
        elif HAS_PyCA and common.IsField3D(defOb):
            defOb = common.AsNPCopy(defOb)
            defOb = NumpyDef(defOb)
        
        assert hasattr(defOb, 'getDefPoint')
        assert hasattr(defOb, 'getDefOffset')
        
        self.defOb = defOb
        self.update()

    def get_2D_pos(self, pos, sdim):
        """
        Return a 2D position from a 3D position and slice dim
        """
        dimIdx = dimmap[sdim]

        return list(pos[0:dimIdx])+list(pos[dimIdx+1:3])

    def update(self):
        self.pos = self.defOb.getDefPoint(self.pos)
        self.off = self.defOb.getDefOffset(self.pos)
        self.pos2D = self.get_2D_pos(self.pos, self.sdim)
        self.off2D = self.get_2D_pos(self.off, self.sdim)
        self.arrow.set_pos(self.pos2D)
        self.arrow.set_off(self.off2D)
    
    def paint(self, p, *args):
        pass

    def boundingRect(self):
        return QtCore.QRectF(0., 0., 0., 0.)

    # slot
    def set_pos(self, ipos):
        self.pos = ipos
        self.update()

    # slot
    def set_dim(self, sdim):
        self.sdim = sdim
        self.update()

class SparseDef(object):
    """
    Represents a sparse deformation, implements getDefPoint and
    getDefOffset
    """

    def __init__(self, corrPos, corrMatch):
        # takes two nCorr-by-2 or nCorr-by-3 arrays, the the first is
        # the correspondence locations and the second are the
        # corresponding points
        assert len(corrPos.shape) == 2
        assert len(corrMatch.shape) == 2
        assert 2 <= corrPos.shape[1] <= 3
        assert 2 <= corrMatch.shape[1] <= 3

        if corrPos.shape[1] == 2:
            # add zero as z-coordinate of correspondences
            corrPos = np.append(corrPos, np.zeros((corrPos.shape[0],1)),axis=1)
        if corrMatch.shape[1] == 2:
            # add zero as z-coordinate of correspondences
            corrMatch = np.append(corrMatch, np.zeros((corrMatch.shape[0],1)),axis=1)

        assert corrPos.shape == corrMatch.shape
        
        self.isOffset = np.sum(np.abs(corrMatch-corrPos)) > \
                        np.sum(np.abs(corrMatch))
        self.corrPos = corrPos
        self.corrMatch = corrMatch

        self.tree = spatial.KDTree(corrPos)

    def getDefPointIdx(self, pos):
        matchDist, matchIdx = self.tree.query(pos, k=1)
        return matchIdx

    def getDefPoint(self, pos):
        matchIdx = self.getDefPointIdx(pos)
        return self.corrPos[matchIdx,:].copy()

    def getDefOffset(self, ipos):
        matchDist, matchIdx = self.tree.query(ipos, k=1)
        off = self.corrMatch[matchIdx,:].copy()
        if not self.isOffset:
            off -= self.corrPos[matchIdx,:]

        return np.array([off[0], off[1], off[2]])

class SparseDefVis(pg.GraphicsObject):
    """
    Creates a DefArrow at each correspondence location.  Position of
    all correspondences is displayed, and the arrow of the closest
    correspondence (to the mouse) is displayed.  Alternately, all
    correspondence arrows can be shown with view_all=True.
    """
    
    def __init__(self,
                 defOb,
                 arrowhead_len=3,
                 arrowhead_angle=math.pi/6.0,
                 ptRad=0.5,
                 view_all=False):

        assert type(defOb) is tuple
        assert len(defOb) == 2
        
        super(SparseDefVis, self).__init__()

        self.view_all = view_all

        corrPoints = defOb[0]
        corrOff = defOb[1]-defOb[0]
        assert type(corrPoints) is np.ndarray
        assert len(corrPoints.shape) == 2
        # list of data objects
        self.dplist = []
        for corrIdx in range(corrPoints.shape[0]):
            pos = corrPoints[corrIdx]
            off = corrOff[corrIdx]
            self.dplist.append(
                DefArrow(draw_arrow=self.view_all,
                         draw_pos=True,
                         pos_rad=ptRad,
                         pos_xrad=ptRad,
                         pos=pos,
                         off=off))
            self.dplist[-1].setParentItem(self)
        self.defOb = SparseDef(defOb[0], defOb[1])
        self.curIdx = 0
    
    def paint(self, p, *args):
        pass
    
    def boundingRect(self):
        return QtCore.QRectF(0, 0, 0, 0)

    def showDef(self, idx):
        self.dplist[idx].show_arrow(True)

    def hideDef(self, idx):
        self.dplist[idx].show_arrow(False)
        
    # slot
    def set_pos(self, ipos):
        if not self.view_all:
            idx = self.defOb.getDefPointIdx(ipos)
            if idx != self.curIdx:
                self.hideDef(self.curIdx)
                self.showDef(idx)
                self.curIdx = idx

    # slot
    def set_dim(self, sdim):
        pass
    
