import pyqtgraph as pg

class InfiniteCrosshair(object):

    def __init__(self, vb, parent=None):
        """
        vb should be a ViewBox
        """
        # crosshair
        self.vb = vb
        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.hLine = pg.InfiniteLine(angle=0, movable=False)
        self.vb.addItem(self.vLine, ignoreBounds=True)
        if parent is not None:
            self.vLine.setParentItem(parent)
        self.vb.addItem(self.hLine, ignoreBounds=True)
        if parent is not None:
            self.hLine.setParentItem(parent)

    # slot
    def set_pos(self, x, y):
        # offset by 0.5 to put crosshair at center of pixel
        self.vLine.setPos(x+0.5)
        self.hLine.setPos(y+0.5)

    def setVisible(self, visible):
        self.vLine.setVisible(visible)
        self.hLine.setVisible(visible)

    def isVisible(self):
        return self.vLine.isVisible()
