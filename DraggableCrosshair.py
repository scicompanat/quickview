import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

class DraggableCrosshair(pg.GraphicsObject):
    """
    A draggable crosshair which emits a 'new_pos' signal when released
    from a drag
    """

    new_pos = QtCore.pyqtSignal(float, float)

    def __init__(self,
                 prad=2, wrad=None,
                 winclr = 'b',
                 patchclr = 'g',
                 draw_patch_box=False,
                 draggable=True):
        super(DraggableCrosshair, self).__init__()
        self.prad = prad
        self.wrad = wrad
        self.winclr = winclr
        self.patchclr = patchclr
        self.draw_patch_box = draw_patch_box
        self.draggable = draggable

    def paint(self, p, *args):
        p.setPen(pg.mkPen(self.patchclr))
        # crosshair
        p.drawLine(-self.prad, 0, self.prad, 0)
        p.drawLine(0, -self.prad, 0, self.prad)
        # patch box
        if self.draw_patch_box:
            p.drawLine(-self.prad, -self.prad, -self.prad, self.prad)
            p.drawLine(-self.prad, self.prad, self.prad, self.prad)
            p.drawLine(self.prad, self.prad, self.prad, -self.prad)
            p.drawLine(self.prad, -self.prad, -self.prad, -self.prad)
        if self.wrad is not None:
            p.setPen(pg.mkPen(self.winclr))
            p.drawLine(-self.wrad, -self.wrad, -self.wrad, self.wrad)
            p.drawLine(-self.wrad, self.wrad, self.wrad, self.wrad)
            p.drawLine(self.wrad, self.wrad, self.wrad, -self.wrad)
            p.drawLine(self.wrad, -self.wrad, -self.wrad, -self.wrad)
    
    def boundingRect(self):
        r = max(self.wrad, self.prad)
        return QtCore.QRectF(-r, -r, 2*r, 2*r)

    def get_pos_wrt_item(self, item):
        pos = self.mapToItem(item,
                             QtCore.QPointF(0.0, 0.0))
        return [pos.x(), pos.y()]
    
    # emits new_pos at end of drag
    def mouseDragEvent(self, ev):
        if not self.draggable:
            return
        ev.accept()
        if ev.isStart():
            self.startPos = self.pos()
        newPos = self.startPos + ev.pos() - ev.buttonDownPos()
        self.setPos(newPos)
        if ev.isFinish():
            self.new_pos.emit(newPos.x(), newPos.y())

    # slot
    def set_pos(self, x, y):
        p = pg.Point(int(x)+0.5, int(y)+0.5)
        self.setPos(p)
        self.new_pos.emit(p.x(), p.y())

    def set_prad(self, prad):
        self.prad=prad
        self.update()

    def set_wrad(self, wrad):
        self.wrad=wrad
        self.update()

