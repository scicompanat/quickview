#!/usr/bin/python
"""
DataManger handles multiple datasets
"""

import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import os
import sys

from . import DefExplorer
from .InfiniteCrosshair import InfiniteCrosshair

try:
    import PyCA.Core as ca
    import PyCA.Common as common
except ImportError:
    ca = None
    common = None

# Mapping from dimension to dim index
dimmap = {'x': 0, 'y': 1, 'z': 2,
          0: 0, 1: 1, 2: 2}

revdimmap = {0: 'X', 1: 'Y', 2: 'Z',
             'x': 'X', 'y': 'Y', 'z': 'Z',
             'X': 'X', 'Y': 'Y', 'Z': 'Z'}

gQApp = None


def _InitQApp():
    """
    Make sure global QApplication has been created and return
    reference to it
    """

    global gQApp

    # try to get an instance of already-created instance
    gQApp = QtGui.QApplication.instance()

    # otherwise create an instance
    if gQApp is None:
        gQApp = QtGui.QApplication([])


def CreateSliceViewWindow(title=None):
    return NewWindow(title)


def NewWindow(title=None):
    """
    Just make sure that a QApplication has been created before
    creating a SliceViewWindow
    """

    _InitQApp()

    svw = SliceViewWindow(title=title)

    return svw


def get_slice(vol, pos=None, dim='z'):
    """
    Return a slice based on the position and slice dim
    """

    dimIdx = dimmap[dim]

    if pos is None:
        pos = [vol.shape[d] // 2 for d in range(3)]

    sliceIdx = pos[dimIdx]

    dimSlices = [slice(None, None, 1) for _ in range(3)]
    dimSlices[dim] = slice(sliceIdx, sliceIdx + 1)
    imslice = vol[dimSlices[0], dimSlices[1], dimSlices[2]]
    return np.squeeze(imslice)


def get_2D_pos(pos, sdim):
    """
    Return a 2D position from a 3D position and slice dim
    """
    dimIdx = dimmap[sdim]

    return list(pos[0:dimIdx]) + list(pos[dimIdx + 1:3])


def calc_histogram(data, bins=500, maxdata=1e5):
    """
    data can be either a numpy array or list of numpy arrays (for
    joint histogram)
    """

    if data is None or \
       (type(data) is list and len(data) == 0):
        return None, None

    if type(data) is list:
        flatData = np.hstack([d.flat for d in data])
    else:
        flatData = data.flat

    rng = (np.min(flatData), np.max(flatData))
    if len(flatData) > maxdata:
        # speed this up by not using all the data
        step = int(np.ceil(float(len(flatData)) / maxdata))
        data = data[::step]

    hist = np.histogram(flatData, bins=bins, range=rng)
    return hist[1][:-1], hist[0]


class DataPosValText(pg.LabelItem):
    """
    Displays a position and data value from current volume in
    DataManager
    """

    def __init__(self, dataman):
        super(DataPosValText, self).__init__('data range')
        self.setAttr('color', 'CCFF00')
        self.setAttr('size', '8pt')
        self.data_manager = dataman
        self.ipos = [0, 0, 0]
        self.data_manager.data_changed.connect(self.update_text)

    def update_text(self, ipos=None):
        if ipos is not None:
            self.ipos = self.data_manager.clamp_to_cur_bounds(ipos)
        else:
            self.ipos = self.data_manager.clamp_to_cur_bounds(self.ipos)
        postext = "({p[0]:d}, {p[1]:d}, {p[2]:d})".format(p=self.ipos)
        vol = self.data_manager.getCurVolume()
        if vol is None:
            self.setText(postext + " = None")
        else:
            v = vol[self.ipos[0], self.ipos[1], self.ipos[2]]
            if hasattr(v, '__len__'):
                if len(v) <= 4:
                    self.setText(postext + " = {v}".format(v=str(v)))
                else:
                    self.setText(postext + " = multival")
            else:
                self.setText(postext + " = {v:f}".format(v=v))


class SliceDimText(pg.LabelItem):
    """
    Displays current slice axis
    """

    def __init__(self):
        super(SliceDimText, self).__init__('DIM: ?')
        self.setAttr('color', '00CCFF')
        self.setAttr('size', '8pt')

    def update_text(self, sdim):
        sdimstr = revdimmap[sdim]
        dimtext = "DIM: {dimstr}".format(dimstr=sdimstr)
        self.setText(dimtext)


class DataVol(object):

    def __init__(self,
                 nparr,
                 name,
                 origin=[0.0, 0.0, 0.0],
                 spacing=[1.0, 1.0, 1.0]):

        self.set_vol(nparr)
        if name is None:
            name = 'no name'
        self.name = name
        self.orign = origin
        self.spacing = spacing

    def set_vol(self, nparr):
        self.vol = np.atleast_3d(nparr)
        self.rng = [np.min(self.vol), np.max(self.vol)]
        self.hist = calc_histogram(self.vol)


class DataManager(QtCore.QObject):
    """
    DataManager handles multiple datasets
    """

    data_changing = QtCore.pyqtSignal()
    data_changed = QtCore.pyqtSignal()

    def __init__(self):
        super(DataManager, self).__init__()
        self.volList = []
        self.ttlRng = [np.inf, -np.inf]
        self.curImIdx = 0
        self.joint_hist_dirty = True

    def nVolumes(self):
        return len(self.volList)

    def addVolume(self, vol, name=None):

        if name is None:
            name = 'Data #%d' % self.nVolumes()

        self.volList.append(DataVol(nparr=vol, name=name))
        vol_rng = self.volList[-1].rng

        self.ttlRng = [min([vol_rng[0], self.ttlRng[0]]),
                       max([vol_rng[1], self.ttlRng[1]])]
        self.joint_hist_dirty = True

        # if it's the first volume, emit a data change
        if self.nVolumes() == 1:
            self.data_changed.emit()

        return len(self.volList) - 1

    def getItem(self, volIdx):

        try:
            item = self.volList[volIdx]
        except IndexError:
            raise Exception('volume index %d out of range' % volIdx)
        return item

    def getVolume(self, volIdx):

        return self.getItem(volIdx).vol

    def updateVolume(self, volIdx, vol):

        volToUp = self.getItem(volIdx)

        volToUp.set_vol(vol)
        vol_rng = volToUp.rng

        self.ttlRng = [min([vol_rng[0], self.ttlRng[0]]),
                       max([vol_rng[1], self.ttlRng[1]])]
        self.joint_hist_dirty = True

        if self.curImIdx == volIdx:
            self.data_changed.emit()

    def getCurItem(self):
        """Get the current DataVol item

        """
        if 0 <= self.curImIdx < self.nVolumes():
            return self.volList[self.curImIdx]
        else:
            return None

    def getCurProp(self, prop):
        """Get a property (attribute) from the current DataVol item

        """
        item = self.getCurItem()
        if item is None:
            return None
        else:
            try:
                val = item.__getattribute__(prop)
            except AttributeError:
                val = None
            return val

    def setCurProp(self, prop, val):
        """Set a property (attribute) on the current DataVol item

        """
        item = self.getCurItem()
        if item is None:
            raise Exception('No current item')
        else:
            item.__setattr__(prop, val)

    def getCurVolume(self):
        """Get numpy array of volume data from current DataVol item

        Returns None if there is no current item
        """
        item = self.getCurItem()
        if item is None:
            return None
        return item.vol

    def getCurName(self):
        item = self.getCurItem()
        if item is None:
            return ''
        return item.name

    def getCurVolIdx(self):
        return self.curImIdx

    def setCurVolume(self, imIdx):
        """Set current DataVol to the volume spacified by imIdx

        emit a data_changed signal if this changes the current volume
        """
        if imIdx < 0 or imIdx >= self.nVolumes():
            raise Exception('image index out of range')
        if imIdx != self.curImIdx:
            self.data_changing.emit()
            self.curImIdx = imIdx
            # emit data_changed signal
            self.data_changed.emit()

    def getTtlRange(self):
        return self.ttlRng

    def getCurRange(self):
        item = self.getCurItem()
        if item is None:
            return [0.0, 0.0]
        return item.rng

    def getCurSz(self):
        vol = self.getCurVolume()
        if vol is not None:
            sz = np.array(vol.shape)
        else:
            sz = np.array([0, 0, 0])
        return sz

    def clamp_to_cur_bounds(self, ipos):
        """
        If there is no volume, return [0,0,0]
        """
        sz = self.getCurSz()
        ipos = list(ipos)
        for d in range(3):
            if ipos[d] >= sz[d]:
                ipos[d] = sz[d] - 1
            if ipos[d] < 0:
                ipos[d] = 0
        return ipos

    def get_cur_hist(self, joint_hist=False, bins=500, maxdata=1e5):
        """
        If there is no volume, return None, None
        """

        if self.nVolumes() == 0:
            return None, None

        if joint_hist:
            if self.joint_hist_dirty:
                data = [v.vol for v in self.volList]
                print('calculating joint histogram...')
                self.joint_hist = \
                    calc_histogram(data, bins=bins, maxdata=maxdata)
                print('done.')
                self.joint_hist_dirty = False
            return self.joint_hist
        else:
            return self.getCurProp('hist')


class VolSlice(pg.ImageItem):
    """
    Manage extracting slice from numpy array as a pg.ImageItem
    """

    def __init__(self, dataman):

        super(VolSlice, self).__init__()
        self.data_manager = dataman
        # current index position
        self.ipos = None
        # current slice dimension
        self.sdim = dimmap['z']
        self.data_manager.data_changed.connect(self._update_data)
        self._update_data()
        self.curRot = 0
        self.sz = np.array([0, 0])
        self.slice_rng = [-1.0, 1.0]
        self.continuous_update = False
        self.last_save_dir = None

    def set_pos(self, ipos):
        """
        update the current 3D position, update the slice if needed
        """
        if self.ipos is None:
            # if self.ipos is None, there's no data in data_manager
            return

        ipos = self.data_manager.clamp_to_cur_bounds(ipos)

        # update if the slice has changed
        if self.ipos[self.sdim] != ipos[self.sdim]:
            self.ipos = ipos
            self._update_data()
        else:
            # have to update this anyway in case slice dim changes
            self.ipos = ipos

    def set_dim(self, sdim):
        """
        update the slice dimension, update the slice if needed
        """
        sdim = dimmap[sdim]
        # update if the slice has changed
        if self.sdim != sdim:
            self.sdim = sdim
            self._update_data()

    def paint(self, p, *args):
        super(VolSlice, self).paint(p, args)
        if self.continuous_update:
            self.update()

    def get_slice_idx(self):
        """
        return the current slice index
        """
        if self.ipos is None:
            return None
        else:
            return self.ipos[self.sdim]

    def get_slice_sz(self):
        sz3D = self.data_manager.getCurSz()
        sz2D = get_2D_pos(sz3D, self.sdim)
        return sz2D

    def get_slice_rng(self):
        return self.slice_rng

    def _update_data(self):
        """
        update slice when something changes
        """
        vol = self.data_manager.getCurVolume()
        if vol is not None:
            if self.ipos is None:
                self.ipos = (np.array(vol.shape) // 2).tolist()
            # update ipos to respect new bouds
            self.ipos = self.data_manager.clamp_to_cur_bounds(self.ipos)
            # extract appropriate slice
            im = get_slice(vol, pos=self.ipos, dim=self.sdim)
            self.sz = np.array(im.shape)
            self.setImage(im, autoLevels=False)
            center = pg.Point(self.sz[0] / 2.0, self.sz[1] / 2.0)
            self.setTransformOriginPoint(center)
            self.slice_rng = [np.min(im), np.max(im)]

    def isInDataBounds(self, spos):
        """
        return True if 2D slice position is in bounds of dataset
        """
        vol = self.data_manager.getCurVolume()
        if vol is None:
            return False

        index = [int(spos[0]), int(spos[1])]
        if index[0] >= 0 and index[0] < self.sz[0] and \
                index[1] >= 0 and index[1] < self.sz[1]:
            return True
        return False

    def closestDataIndex(self, spos):
        vol = self.data_manager.getCurVolume()
        if vol is None:
            return [0, 0]
        sz2D = get_2D_pos(vol.shape, self.sdim)
        ppos = self.mapFromParent(spos[0], spos[1])
        spos[0] = min(max(int(ppos.x()), 0), sz2D[0] - 1)
        spos[1] = min(max(int(ppos.y()), 0), sz2D[1] - 1)
        return spos

    def ipos_from_spos(self, spos):
        """
        convert a 2D index position in this slice to a 3D index
        position in the corresponding volume
        """
        sidx = self.get_slice_idx()
        if sidx is None:
            sidx = 0
        ipos = list(spos)
        ipos.insert(self.sdim, sidx)
        return ipos

    def continuousUpdate(self, up=None):
        if up is None:
            return self.continuous_update
        else:
            up = bool(up)
            if up != self.continuous_update:
                self.continuous_update = up
                if self.continuous_update:
                    # schedule an initial update
                    self.update()

    def reset(self):
        self.resetTransform()
        # rotation is separate from transform
        self.setRotation(0)

    def rotate(self, k=1):
        self.curRot = (self.curRot + k) % 4
        self.setRotation(90.0 * self.curRot)

    def flipud(self):
        tx = QtGui.QTransform.fromScale(1.0, -1.0)
        tx = tx.translate(0.0, -self.sz[1])
        self.setTransform(tx, combine=True)

    def fliplr(self):
        tx = QtGui.QTransform.fromScale(-1.0, 1.0)
        tx = tx.translate(-self.sz[0], 0.0)
        self.setTransform(tx, combine=True)

    def fileSaveDialog(self, opts=None):
        # Show a file dialog, saving this image when finished.
        if opts is None:
            opts = {}
        # pg.widgets.FileDialog is just minimal wrapper around QFileDialog
        self.fileDialog = pg.widgets.FileDialog.FileDialog()
        self.fileDialog.setFileMode(QtGui.QFileDialog.AnyFile)
        self.fileDialog.setAcceptMode(QtGui.QFileDialog.AcceptSave)
        exportDir = self.last_save_dir
        if exportDir is not None:
            self.fileDialog.setDirectory(exportDir)
        self.fileDialog.show()
        self.fileDialog.opts = opts
        self.fileDialog.fileSelected.connect(self.fileSaveFinished)

    def fileSaveFinished(self, fileName):
        self.last_save_dir = os.path.split(str(fileName))[0]
        self.save(fileName)


class SliceDisplay(pg.ViewBox):
    """
    Displays a slice, emits and receives mouse signals,
    handles converting to dataset index coordinates
    """

    new_mouse_pos = QtCore.pyqtSignal(list)
    new_slice_dim = QtCore.pyqtSignal(int)

    next_slice_keys = \
        [QtCore.Qt.Key_Period,
         QtCore.Qt.Key_Greater,
         QtCore.Qt.Key_Up]

    prev_slice_keys = \
        [QtCore.Qt.Key_Comma,
         QtCore.Qt.Key_Less,
         QtCore.Qt.Key_Down]

    dim_keymap = {QtCore.Qt.Key_X: 'x',
                  QtCore.Qt.Key_Y: 'y',
                  QtCore.Qt.Key_Z: 'z'}

    reset_keys = [QtCore.Qt.Key_R]

    def __init__(self, dataman):
        super(SliceDisplay, self).__init__()
        self.data_manager = dataman
        self.slice = VolSlice(self.data_manager)
        self.SetupGUI()

        # allow this widget to catch keyPressEvents
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setFocus()

    def SetupGUI(self):

        self.setAspectLocked()  # image aspect

        self.addItem(self.slice)
        self.autoRange()

        #
        # mouse position cross hair
        #
        self.mch = InfiniteCrosshair(self, self.slice)

    def mpos_to_spos(self, mpos):
        """
        Convert a mouse position to dataset index coords
        """
        mousePoint = self.mapSceneToView(mpos)
        spos = [mousePoint.x(), mousePoint.y()]
        return spos

    # slot, gets qt mouse movement event emits new_mouse_pos signal
    def mouse_moved(self, evt):
        """
        slot to receive sigMouseMoved from ViewBox.scene(), handles
        mapping mouse to image indices and emits new signal with image
        indices
        """

        # using signal proxy turns original arguments into a tuple
        mpos = evt[0]

        spos = self.mpos_to_spos(mpos)
        spos = self.slice.closestDataIndex(spos)
        ipos = self.slice.ipos_from_spos(spos)
        self.new_mouse_pos.emit(ipos)

    # slot for updating mouse pos
    def set_mouse_pos(self, ipos):
        """
        slot that changes crosshair mouse position
        """
        spos = get_2D_pos(ipos, sdim=self.slice.sdim)
        self.mch.set_pos(int(spos[0]), int(spos[1]))

    def connect_signals(self, autoconn=False):
        # have mouse movements call mouse_moved slot, which emits a
        # new_mouse_pos signal
        self.proxy = pg.SignalProxy(self.scene().sigMouseMoved,
                                    rateLimit=60, slot=self.mouse_moved)

        # connect internal signals and slots -- just use this if this
        # is the only LayerDisplay
        if autoconn:
            # signal/slot connections
            self.new_mouse_pos.connect(self.set_mouse_pos)
            self.new_mouse_pos.connect(self.slice.set_pos)
            self.new_slice_dim.connect(self.slice.set_dim)

    def set_slice_dim(self, sdim):
        self.new_slice_dim.emit(dimmap[sdim])

    def set_slice(self, sidx):
        ipos = list(self.slice.ipos)
        ipos[self.slice.sdim] = sidx
        ipos = self.data_manager.clamp_to_cur_bounds(ipos)
        self.new_mouse_pos.emit(ipos)

    def next_slice(self):
        curslice = self.slice.get_slice_idx()
        if curslice is not None:
            self.set_slice(curslice + 1)

    def prev_slice(self):
        curslice = self.slice.get_slice_idx()
        if curslice is not None:
            self.set_slice(curslice - 1)

    def connect_hist_lut(self, histlut):
        histlut.sigLookupTableChanged.connect(self._histlut_changed)
        histlut.sigLevelsChanged.connect(self._levels_changed)
        # hist.ut.sigLevelChangeFinished.connect(self.????)

    def _histlut_changed(self, histlut):
        if histlut.gradient.isLookupTrivial():
            self.slice.setLookupTable(None)
        else:
            self.slice.setLookupTable(histlut.getLookupTable)

    def _levels_changed(self, histlut):
        self.slice.setLevels(histlut.region.getRegion())

    def reset_view(self):
        if self.data_manager.nVolumes() == 0:
            return
        # slice transform (resetTransform) isn't related to view
        # modifications
        # self.slice.slice.resetTransform()
        sz = self.slice.get_slice_sz()
        self.setRange(xRange=[0, sz[0]], yRange=[0, sz[1]],
                      padding=None, update=True,
                      disableAutoRange=True)
        # bounds = self.rect()
        # center = bounds.center()
        # m = QtGui.QTransform()
        # m.translate(center.x(), center.y())
        # m.scale(1.0, 1.0)
        # self.childGroup.setTransform(m)

    def keyPressEvent(self, e):

        key = e.key()
        mod = e.modifiers()
        nomod = (mod is None or mod == QtCore.Qt.NoModifier)
        shiftmod = (mod == QtCore.Qt.ShiftModifier)
        ctrlmod = (mod == QtCore.Qt.ControlModifier)

        if key in self.next_slice_keys and nomod:
            self.next_slice()
        elif key in self.prev_slice_keys and nomod:
            self.prev_slice()
        elif key in self.next_slice_keys and ctrlmod:
            # go to last slice
            sdim_max = self.data_manager.getCurSz()[self.slice.sdim]
            self.set_slice(sdim_max)
        elif key in self.prev_slice_keys and ctrlmod:
            # go to first slice
            self.set_slice(0)
        elif key in self.dim_keymap and nomod:
            self.set_slice_dim(self.dim_keymap[key])
        elif key == QtCore.Qt.Key_F and nomod:
            self.slice.fliplr()
        elif key == QtCore.Qt.Key_F and shiftmod:
            self.slice.flipud()
        elif key == QtCore.Qt.Key_G and nomod:
            self.slice.rotate()
        elif key in self.reset_keys and nomod:
            self.reset_view()
            # allow reset to propogate up
            super(SliceDisplay, self).keyPressEvent(e)
        elif key in self.reset_keys and shiftmod:
            self.reset_view()
            self.slice.reset()
            # allow reset to propogate up
            super(SliceDisplay, self).keyPressEvent(e)
        elif key == QtCore.Qt.Key_C and nomod:
            self.mch.setVisible(not self.mch.isVisible())
            # toggle continuous update
            # self.slice.continuousUpdate(not self.slice.continuousUpdate())
        elif key == QtCore.Qt.Key_S and ctrlmod:
            self.slice.fileSaveDialog()
        else:
            super(SliceDisplay, self).keyPressEvent(e)


class SliceViewWidget(QtGui.QWidget):

    toggle_joint_range_keys = [QtCore.Qt.Key_J]

    help_keys = [QtCore.Qt.Key_H]

    reset_keys = [QtCore.Qt.Key_R]

    helptext = """
    <,up - next slice for 3D data
    <,down - previous slice for 3D data
    1-9 - switch to indexed dataset
    left/right - previous / next dataset
    ctrl + 1-9 - set current dataset to given key
    f/F - flip view horizontally / vertically
    g - rotate view 90 degrees
    G - rotate view -90 degrees
    x,y,z - choose slice dimension
    j - toggle joint range
    r - reset view
    R - reset view and flips/rotations
    s - reset window level to current slice range
    l - toggle log histogram
    ctrl+s - save current image (as colormapped)
    c - toggle mouse crosshair
    q/ESC - quit
    h - display this help
    """

    def __init__(self, imlist=[], namelist=None):

        super(SliceViewWidget, self).__init__()

        # mapping of key to data set index
        self.key_map = dict([(v, (v - 1) % 10) for v in range(10)])

        self.data_manager = DataManager()
        self.joint_range = False
        self.use_log_hist = False
        # wrap image in list if single image passed in
        if type(imlist) is np.ndarray:
            imlist = [imlist]

        # None is fine if no namelist
        if namelist is None:
            namelist = [None for _ in imlist]

        # wrap image in list if single name passed in
        if type(namelist) is str:
            namelist = [namelist]

        if len(namelist) != len(imlist):
            raise Exception('length of namelist must match length of imlist')

        nImages = len(imlist)
        # add images
        for imIdx in range(nImages):
            self.data_manager.addVolume(imlist[imIdx], name=namelist[imIdx])
        self.SetupGUI()

    def SetupGUI(self):

        self.qlayout = QtGui.QGridLayout()
        self.setLayout(self.qlayout)
        self.qlayout.setSpacing(0)

        self.glayout = pg.GraphicsLayout()

        self.data_name = self.glayout.addLabel('image', col=0, row=0)
        self.glayout.nextRow()

        self.glayout_view = pg.GraphicsView()
        self.glayout_view.setCentralItem(self.glayout)
        # self.glayout_view.show()
        self.glayout_view.setWindowTitle('pyqtgraph example: GraphicsLayout')
        self.glayout_view.resize(800, 600)

        self.slice = SliceDisplay(self.data_manager)
        self.glayout.addItem(self.slice, col=0)

        # set up bottom text display bar
        self.textlayout = pg.GraphicsLayout()
        self.textlayout.setSizePolicy(
            QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding,
                              QtGui.QSizePolicy.Minimum))
        self.dimtext = SliceDimText()
        self.textlayout.addItem(self.dimtext, col=0)
        self.slice.new_slice_dim.connect(
            self.dimtext.update_text)
        self.pvtext = DataPosValText(self.data_manager)
        self.textlayout.addItem(self.pvtext, col=1)
        self.glayout.nextRow()
        self.glayout.addItem(self.textlayout, col=0)

        self.ge = pg.HistogramLUTItem()
        self.glayout.addItem(self.ge, row=0, col=1, rowspan=3)
        self.data_manager.data_changing.connect(self.save_data_settings)
        self.data_manager.data_changed.connect(self.update_hist)
        self.slice.connect_hist_lut(self.ge)
        # add the graphical layout to the qt layout
        self.qlayout.addWidget(self.glayout_view, 0, 0)
        self.data_manager.data_changed.connect(self.data_changed)
        self.update_hist()

        self.slice.connect_signals(autoconn=True)
        self.slice.new_mouse_pos.connect(self.pvtext.update_text)

        # emit an initial slice changed event
        self.slice.set_slice_dim(self.slice.slice.sdim)

    def add_volume(self, vol, name=None):
        firstvol = (self.data_manager.nVolumes() == 0)
        idx = self.data_manager.addVolume(vol=vol, name=name)
        if firstvol:
            self.slice.reset_view()
        return idx

    def set_volume(self, idx, vol):
        self.data_manager.updateVolume(idx, vol=vol)

    def get_volume(self, idx):
        return self.data_manager.getVolume(idx)

    def set_field(self, f, view_all=True):
        # create sparse or dense deformation
        if type(f) is tuple:
            self.field_vis = DefExplorer.SparseDefVis(
                defOb=f, view_all=view_all)
        else:
            self.field_vis = DefExplorer.MovingArrow(defOb=f)

        self.slice.new_mouse_pos.connect(
            self.field_vis.set_pos)
        self.slice.new_slice_dim.connect(
            self.field_vis.set_dim)
        self.slice.addItem(self.field_vis)
        self.field_vis.setParentItem(self.slice.slice)

    def use_joint_range(self, jr=None):
        if jr is None:
            return self.joint_range

        if jr != self.joint_range:
            self.joint_range = jr
            self.update_hist()

    def save_data_settings(self):
        cur_levels = self.ge.region.getRegion()
        self.data_manager.setCurProp('win_levels', cur_levels)

    def update_hist(self):
        """
        update the displayed histogram in the histlut, but leave
        current range (window levels) the same
        """
        if self.data_manager.nVolumes() > 0:
            h = self.data_manager.get_cur_hist(joint_hist=self.joint_range)
            if self.use_log_hist:
                h = (h[0], np.log(h[1] + 1.0))
            self.ge.plot.setData(*h)

    def reset_hist(self, last_levels=False):
        """
        reset the window levels to the full histogram range
        """
        if self.data_manager.nVolumes() > 0:
            levels = None

            if last_levels:
                levels = self.data_manager.getCurProp('win_levels')

            if levels is None:
                # set to full range
                levels = self.ge.plot.dataBounds(ax=0)
            self.ge.region.setRegion(levels)
            # reset the view of the histogram
            self.ge.plot.getViewBox().autoRange()

    def _levels_changed(self):
        """
        directly call _levels_changed slot of SliceDisplay
        """
        self.slice._levels_changed(self.ge)

    def data_changed(self):
        """
        slot for DataManager data_changed signal
        """

        self.update_hist()
        if not self.joint_range:
            self.reset_hist(last_levels=True)
        self._levels_changed()

        volIdx = self.data_manager.getCurVolIdx()
        volName = self.data_manager.getCurName()
        volRng = self.data_manager.getCurItem().rng
        volShape = self.data_manager.getCurVolume().shape
        if len(volShape) <= 3:
            volSzStr = str(volShape)
            nChannelsStr = "(scalar)"
        else:
            volSzStr = str(volShape[:3])
            nChannelsStr = "(vector: {v})".format(v=str(volShape[3:]))

        text = '{volIdx:d}: {volName}<br/>' \
               'size {volSz} {nChannels}<br/>' \
               'data range: ({rng[0]:f}, {rng[1]:f})'
        text = text.format(volIdx=volIdx, volName=volName,
                           volSz=volSzStr, nChannels=nChannelsStr,
                           rng=volRng)

        self.data_name.setText(text)

    def setBackgroundColor(self, color):
        """
        Set color as triple of ints ie (255, 255, 255) is white
        """
        self.slice.setBackgroundColor(color)

    def keyPressEvent(self, e):

        key = e.key()
        mod = e.modifiers()

        nomod = (mod is None or mod == QtCore.Qt.NoModifier)
        shiftmod = (mod == QtCore.Qt.ShiftModifier)
        ctrlmod = (mod == QtCore.Qt.ControlModifier)

        if QtCore.Qt.Key_0 <= key <= QtCore.Qt.Key_9 and nomod:
            numkey = int(chr(key))
            try:
                self.data_manager.setCurVolume(self.key_map[numkey])
            except Exception as ex:
                print(ex)
                print('No dataset ', numkey)
        if QtCore.Qt.Key_0 <= key <= QtCore.Qt.Key_9 and ctrlmod:
            numkey = int(chr(key))
            self.key_map[numkey] = self.data_manager.getCurVolIdx()
        elif key == QtCore.Qt.Key_Right and nomod:
            curIdx = self.data_manager.getCurVolIdx()
            nVol = self.data_manager.nVolumes()
            volIdx = (curIdx + 1) % nVol
            if volIdx != curIdx:
                self.data_manager.setCurVolume(volIdx)
        elif key == QtCore.Qt.Key_Left and nomod:
            curIdx = self.data_manager.getCurVolIdx()
            nVol = self.data_manager.nVolumes()
            volIdx = (curIdx - 1) % nVol
            if volIdx != curIdx:
                self.data_manager.setCurVolume(volIdx)
        elif key in self.toggle_joint_range_keys and nomod:

            self.use_joint_range(not self.use_joint_range())

        elif key in self.reset_keys and nomod:
            self.reset_hist()
            self.slice.reset_view()
        elif key in self.reset_keys and shiftmod:
            self.reset_hist()
            self.slice.reset_view()
            self.slice.slice.reset()
        elif key == QtCore.Qt.Key_S and nomod:
            # set window level to slice range
            print('resetting slice range')
            rng = self.slice.slice.get_slice_rng()
            self.ge.region.setRegion(rng)
        elif key == QtCore.Qt.Key_L and nomod:
            # toggle log histogram
            self.use_log_hist = not self.use_log_hist
            print('log histogram ' + ('on' if self.use_log_hist else 'off'))
            self.update_hist()
            self.ge.plot.getViewBox().autoRange()
        elif key in self.help_keys and nomod:
            helpmessage = QtGui.QMessageBox(QtGui.QMessageBox.NoIcon,
                                            'help',
                                            self.helptext)
            helpmessage.show()
            helpmessage.exec_()

        else:
            super(SliceViewWidget, self).keyPressEvent(e)


class SliceViewWindow(QtGui.QMainWindow):
    """
    SliceViewWindow wraps a SliceViewWidget in a QMainWindow and
    handles close events
    """

    quit_keys = [QtCore.Qt.Key_Q, QtCore.Qt.Key_Escape]

    def __init__(self, title=None):
        super(SliceViewWindow, self).__init__()
        self.title = title
        self.SetupGUI()

    # what to do when the user clicks 'x'
    def closeEvent(self, e):
        e.accept()

    def SetupGUI(self):

        self.resize(800, 600)
        self.svw = SliceViewWidget()
        self.setCentralWidget(self.svw)
        if self.title is not None:
            self.title = 'QuickView: ' + str(self.title)
        else:
            self.title = 'QuickView'
        self.setWindowTitle(self.title)
        # self.show()

    def add_volume(self, vol, name=None):
        if ca is not None and isinstance(vol, ca.Image3D):
            vol = common.AsNPCopy(vol)
        return self.svw.add_volume(vol, name=name)

    def set_volume(self, idx, vol):
        if ca is not None and isinstance(vol, ca.Image3D):
            vol = common.AsNPCopy(vol)
        self.svw.set_volume(idx, vol)

    def get_volume(self, idx):
        return self.svw.get_volume(idx)

    def set_field(self, f, view_all=True):
        self.svw.set_field(f, view_all=view_all)

    def setBackgroundColor(self, color):
        """
        Set color as triple of ints ie (255, 255, 255) is white
        """
        self.svw.setBackgroundColor(color)

    def keyPressEvent(self, e):

        key = e.key()
        mod = e.modifiers()
        nomod = (mod is None or mod == QtCore.Qt.NoModifier)

        if key in self.quit_keys and nomod:
            # QtGui.QApplication.closeAllWindows()
            self.close()
        else:
            super(SliceViewWindow, self).keyPressEvent(e)

# Start Qt event loop unless running in interactive mode.


def StartEventLoop(force=False):

    # test if we're in ipython
    try:
        from IPython.core.interactiveshell import InteractiveShell
    except ImportError:
        using_ipython = False
    else:
        using_ipython = InteractiveShell.initialized()

    # test if running from interactive python session
    python_shell = hasattr(sys, 'ps1')

    # sys.flags.interactive set if python is run with -i flag
    interactive = using_ipython or python_shell or sys.flags.interactive

    if force or not interactive:
        print('starting exec')
        QtGui.QApplication.instance().exec_()

if __name__ == '__main__':

    try:
        import PyCA.Common as common
        HAS_PyCA = True
    except ImportError:
        HAS_PyCA = False

    try:
        from PIL import Image
        HAS_PIL = True
    except ImportError:
        HAS_PIL = False

    try:
        import matplotlib.pyplot as plt
        HAS_PLT = True
    except ImportError:
        HAS_PLT = False

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("image", help="frame file to display",
                        type=str, nargs='+')
    parser.add_argument("-f", "--field", help="vector field to use",
                        type=str, default='')
    parser.add_argument("--force_scalar",
                        help="interpret fields as 3D scalar instead of e.g. 2D RGB",
                        action='store_true')
    parser.add_argument("--bgcolor",
                        help="set the background color of the window",
                        type=str)
    args = parser.parse_args()

    print(args.image)

    svw = CreateSliceViewWindow()
    for imfile in args.image:
        if imfile.endswith('.npy'):
            im = np.load(imfile)
        elif HAS_PyCA:
            im = common.LoadImage(imfile)
            im = common.AsNPCopy(im)
        elif HAS_PIL:
            im = Image.open(imfile)
            im = np.array(im)
        elif HAS_PLT:
            im = plt.imread(imfile)

        # test if this is (probably) an RGB or RGBa image
        if not args.force_scalar:
            if len(im.shape) == 3 and 3 <= im.shape[2] <= 4:
                im = im[:, :, None, :]
        svw.add_volume(im, name=imfile)
    if len(args.field):
        if not HAS_PyCA:
            raise Exception('field only available with PyCA support')
        f = common.LoadITKField(args.field)
        farr = common.AsNPCopy(f)
        f = common.FieldFromNPArr(farr)
        svw.set_field(f)
    if args.bgcolor is not None:
        # not safe, but easy
        bgcolor = eval(args.bgcolor)
        svw.setBackgroundColor(bgcolor)

    svw.show()
    StartEventLoop()
