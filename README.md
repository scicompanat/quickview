# Notes 

Simple viewer for multiple 2D/3D datasets based on the pyqtgraph
python module. Allows joint window leveling and toggling between
datasets with 0-9 keys. Meant as an enhanced replacement for ITK's
ImageViewer, which can be run as a stand-alone application (using
PyCA's image IO functionality) or interactively from within
IPython. The core module does not depend on PyCA and takes numpy
arrays as input.

QuickView was developed using pyqtgraph 0.9.8 

Press 'h' for help dialog

# Quick list of current and planned features:

- text display:
    - *[DONE]* total dimensions
    - *[DONE]* mouse position (current pixel location) 
    - *[DONE]* current pixel value
    - *[DONE]* total value range
    - current window range
    - *[DONE]* filename
    - *[DONE]* current slice dimension

- ability to:
    - *[DONE]* display different axes
    - *[DONE]* adjust window levels
    - *[DONE]* open multiple files 
        - *[DONE]* switch between them (keeping current view) with [0-9] keys
    - step through third dimension with keys or slider
    - handle spacing and origin
    - toggle use of spacing and origin
    - *[DONE]* ability to display z dimension as RGB instead of multiple slices
    - ability to save displayed images
   
## TODO:
- *[DONE]* dataset hotkeys should be 1-0 not 0-9 
- *[DONE]* dataset should initialize with center slice
- additional draggable crosshair should define slice pos, not mouse
- mouse crosshair should change color if mouse is not in current slice (for 3-pane view)
- *[DONE]* start with SliceDisplay (ViewBox) focused
- *[DONE]* quit key
- *[DONE]* reset key
- *[DONE]* display current dataset index/filename
- *[DONE]* fix: initial data range wrong in histogram
- histogram dominated by zero region -- need ability to look at sub-section of histogram
- *[DONE]* data range should stay the same when switching between datasets?
- *[DONE]* help display
- *[DONE]* remove dependencies on PyCA -- accept only numpy arrays, let loading them be a separate issue
- *[DONE]* transpose / flip / rotate images
- ability to reset flip / rotation
- ability to flip / rotate datasets independently of each other
- allow separate data ranges for each dataset?
